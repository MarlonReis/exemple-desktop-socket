package br.com.keymax.connection;


import org.springframework.messaging.converter.SimpleMessageConverter;
import org.springframework.messaging.simp.stomp.StompFrameHandler;
import org.springframework.messaging.simp.stomp.StompHeaders;
import org.springframework.messaging.simp.stomp.StompSession;
import org.springframework.messaging.simp.stomp.StompSessionHandlerAdapter;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;
import org.springframework.web.socket.messaging.WebSocketStompClient;
import org.springframework.web.socket.sockjs.client.SockJsClient;
import org.springframework.web.socket.sockjs.client.WebSocketTransport;
import org.springframework.web.socket.sockjs.client.Transport;

import java.lang.reflect.Type;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;


public class Teste {
    public static void main(String[] args) throws URISyntaxException {
        Teste teste = new Teste();
        teste.startConnectionSocket();
    }
    public void startConnectionSocket() throws URISyntaxException {
        URI stompUrlEndpoint = new URI("ws://localhost:8080/gs-guide-websocket");

        ThreadPoolTaskScheduler taskScheduler = new ThreadPoolTaskScheduler();

        taskScheduler.afterPropertiesSet();

        StandardWebSocketClient webSocketClient = new StandardWebSocketClient();
        List<Transport> transports = new ArrayList<Transport>();

        transports.add(new WebSocketTransport(webSocketClient));

        SockJsClient sockJsClient = new SockJsClient(transports);

        WebSocketStompClient stompClient = new WebSocketStompClient(sockJsClient);

        stompClient.setMessageConverter(new SimpleMessageConverter());          // default converter: SimpleMessageConverter


        stompClient.setTaskScheduler(taskScheduler);


        StompSessionHandlerImp stompSessionHandlerImp = new StompSessionHandlerImp();

        ListenableFuture<StompSession> stompSessionFuture = stompClient.connect(stompUrlEndpoint.toString(), stompSessionHandlerImp);


        try {
            StompSession stompSession = stompSessionFuture.get(30, TimeUnit.SECONDS);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private class StompSessionHandlerImp extends StompSessionHandlerAdapter {
        private StompSession session;


        @Override
        public void afterConnected(StompSession session, StompHeaders connectedHeaders) {
            this.session = session;

            session.setAutoReceipt(true);
            session.subscribe("/topic/greetings", new StompFrameHandler() {
                public Type getPayloadType(StompHeaders stompHeaders) {
                    System.out.println("getPayloadType");
                    return null;
                }

                public void handleFrame(StompHeaders stompHeaders, Object o) {
                    System.out.println("handleFrame");
                }
            });
        }
    }

    public class MySessionHandler extends StompSessionHandlerAdapter {
        @Override
        public void afterConnected(StompSession session, StompHeaders connectedHeaders) {
            System.out.println("afterConnected");
        }
    }


}
